sumaimpares = 0
numero1 = int(input("dame un numero entero no negativo: "))
if numero1 < 0:
    numero1 = int(input("dame un numero entero NO negativo"))
else:
    numero2 = int(input("dame otro numero entero no negativo: "))
    if numero2 < 0:
        numero2 = int(input("dame un numero entero NO negativo"))
    else:
        n1 = min(numero1, numero2)
        n2 = max(numero1, numero2)

        if n1 % 2 != 0:
            sumaimpares += n1
        else:
            sumaimpares = sumaimpares
        for i in range(n1 + 1, n2):
            if i % 2 != 0:
                sumaimpares += i
            else:
                sumaimpares = sumaimpares
        if n2 % 2 != 0:
            sumaimpares += n2
        else:
            sumaimpares = sumaimpares

print("la suma de los impares comprendidos entre", n1, "y", n2, "es: ", sumaimpares)